# WARNING
The project has been moved to [UBPorts - Community Ports](https://gitlab.com/ubports/community-ports/android10/samsung-galaxy-s10). Please use that instead. This is available for historical purposes.

# Ubuntu Touch for Samsung Galaxy S10, S10e, S10+ Exynos (beyondxlte)

# Status
* Cameras do not work.
* Ringtones do not work.
* Sound works by removing `flags="AUDIO_OUTPUT_FLAG_MULTI_CH"` on `/vendor/etc/audio_policy_configuration.xml`
* File picker do not work.

Please make a issue for things that do not work that *arent* on this list.

# Building

To build by hand, run these commands;

```
./build.sh -b bd  # bd is the name of the build directory
./build/prepare-fake-ota.sh out/device_violet.tar.xz ota
./build/system-image-from-ota.sh ota/ubuntu_command out
```

# Installation

To install, follow these steps;

- Flash [this](https://forum.xda-developers.com/t/rom-exynos-lineageos-18-1-for-s10e-s10-s10-v2-2-update-2021-07-23.4076585/) rom (Make sure that you flash the 17.1 version of it)
- Wipe data and system
- Flash ubuntu.img onto system
- Flash boot.img onto boot
- (optional) to make sound work, remove any signs of `flags="AUDIO_OUTPUT_FLAG_MULTI_CH"` off of `/vendor/etc/audio_policy_configuration.xml`

